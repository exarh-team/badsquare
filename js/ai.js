var who;

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function ai_start(w) {
    console.debug("ход бота");

    who = w;

    ai_do_something(1);

    checkFinish();

    changeTeamEnergy();
    changeTeam();
}

function ai_do_something(attempt) {
    var from, data;
    var priorities = [];

    for (from=0; from<64; from++) {

        if ((tileItems[from].type==="unit")&&(tileItems[from].team===who)) {
            data = ai_radar(from);
            priorities = priorities.concat(ai_getPriorities(data, from, tileItems[from].isBoss));
        }

    }

    var init = 1;
    var length = priorities.length;
    var elem, stepTo;

    while ((init < 10)&&(teamEnergy[who]>0)) {

        for (var i=0; i < length; i++) {

            if (priorities[i][0] === init) {
                elem = priorities[i];

                highlightActions(elem[2]);

                stepTo = checkMoveUnit(elem[2], elem[3], true);
                if ((elem[1] === "move")&&(stepTo !== false)) {
                    moveUnit(elem[2], stepTo);

                    highlightTeam();
                    if ((teamEnergy[who]>0)&&(attempt>0)) {
                        ai_do_something(attempt-1);
                    }
                }

                if ((elem[1] === "attack")&&(checkAttackUnit(elem[2], elem[3]))) {
                    attack(elem[2], elem[3]);
                }

                highlightTeam();
            }
        }

        init += 1;
    }

    if ((teamEnergy[who]>0)&&(attempt>0)) {
        ai_do_something(attempt-1);
    }
}

function ai_radar(ind) {
    var i, j, lside, rside, distance, target, gear;
    var data = [ [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0] ];

    i = Math.floor(ind/8);
    j = ind%8;

    // по левой стороне всегда без отступа при первом просчете,
    // чтобы не было наложения правой стороной при разведке всех четырех векторов
    // если изобразить, какие клетки проверяются, выглядит как лопасти вентилятора

    data = ai_radar_top(data, i, j);
    data = ai_radar_right(data, i, j);
    data = ai_radar_bottom(data, i, j);
    data = ai_radar_left(data, i, j);

    return data;
}

function isUnit(ind, team) {
    return (
            (tileItems[ind] !== undefined)&&
            (tileItems[ind].type === "unit")&&
            (tileItems[ind].team === team)&&
            (tileItems[ind].xp > 0)
        );
}

function isWall(ind) {
    return (
            (tileItems[ind] !== undefined)&&
            (tileItems[ind].type === "wall")
        );
}



function ai_radar_top(data, i, j) {
    var gear, target;
    i -= 1;
    var lside = j;
    var rside = j+1;

    var distance = 1;
    while (((data[0][0] === 0)||(data[0][1] === 0))&&(distance<8)&&(i>=0)) {

        if (lside<0)  lside = 0;
        if (lside>=7) lside = 7;
        if (rside<0)  rside = 0;
        if (rside>=7) rside = 7;

        for (gear=lside; gear<=rside; gear++) {

            target = i*8+gear;

            if ((data[0][0] === 0) && isUnit(target, ((who === "blue") ? "blue" : "green"))) {
                data[0][0] = [distance, target];
            }

            if ((data[0][1] === 0) && isUnit(target, ((who !== "blue") ? "blue" : "green"))) {
                data[0][1] = [distance, target];
            }

            if ((data[0][2] === 0) && isWall(target)) {
                data[0][2] = [distance, target];
            }
        }

        lside -= 1;
        rside += 1;
        i -= 1;

        distance += 1;
    }
    return data;
}

function ai_radar_right(data, i, j) {
    var gear, target;
    j += 1;
    var lside = i;
    var rside = i+1;

    var distance = 1;
    while (((data[1][0] === 0)||(data[1][1] === 0))&&(distance<8)&&(j<8)) {

        if (lside<0)  lside = 0;
        if (lside>=7) lside = 7;
        if (rside<0)  rside = 0;
        if (rside>=7) rside = 7;

        for (gear=lside; gear<=rside; gear++) {

            target = gear*8+j;

            if ((data[1][0] === 0) && isUnit(target, ((who === "blue") ? "blue" : "green"))) {
                data[1][0] = [distance, target];
            }

            if ((data[1][1] === 0) && isUnit(target, ((who !== "blue") ? "blue" : "green"))) {
                data[1][1] = [distance, target];
            }

            if ((data[1][2] === 0) && isWall(target)) {
                data[1][2] = [distance, target];
            }
        }

        lside -= 1;
        rside += 1;
        j += 1;

        distance += 1;
    }
    return data;
}

function ai_radar_bottom(data, i, j) {
    var gear, target;
    i += 1;
    var lside = j;
    var rside = j-1;

    var distance = 1;
    while (((data[2][0] === 0)||(data[2][1] === 0))&&(distance<8)&&(i<8)) {

        if (lside<0)  lside = 0;
        if (lside>=7) lside = 7;
        if (rside<0)  rside = 0;
        if (rside>=7) rside = 7;

        for (gear=lside; gear>=rside; gear--) {

            target = i*8+gear;

            if ((data[2][0] === 0) && isUnit(target, ((who === "blue") ? "blue" : "green"))) {
                data[2][0] = [distance, target];
            }

            if ((data[2][1] === 0) && isUnit(target, ((who !== "blue") ? "blue" : "green"))) {
                data[2][1] = [distance, target];
            }

            if ((data[2][2] === 0) && isWall(target)) {
                data[2][2] = [distance, target];
            }
        }

        lside += 1;
        rside -= 1;
        i += 1;

        distance += 1;
    }
    return data;
}

function ai_radar_left(data, i, j) {
    var gear, target;
    j -= 1;
    var lside = i;
    var rside = i-1;

    var distance = 1;
    while (((data[3][0] === 0)||(data[3][1] === 0))&&(distance<8)&&(j>0)) {

        if (lside<0)  lside = 0;
        if (lside>=7) lside = 7;
        if (rside<0)  rside = 0;
        if (rside>=7) rside = 7;

        for (gear=lside; gear>=rside; gear--) {

            target = gear*8+j;

            if ((data[3][0] === 0) && isUnit(target, ((who === "blue") ? "blue" : "green"))) {
                data[3][0] = [distance, target];
            }

            if ((data[3][1] === 0) && isUnit(target, ((who !== "blue") ? "blue" : "green"))) {
                data[3][1] = [distance, target];
            }

            if ((data[3][2] === 0) && isWall(target)) {
                data[3][2] = [distance, target];
            }
        }

        lside += 1;
        rside -= 1;
        j -= 1;

        distance += 1;
    }
    return data;
}



function ai_getPriorities(data, from, isBoss) {
    var priorities = [];

    for (var i=0;i<4;i++) {
        if (((data[i][1] !== 0))&&((data[i][1][0] === 1)||(isBoss))) {
            priorities.push([data[i][1][0], "attack", from, data[i][1][1]]);
        }
        if (((data[i][1] !== 0))&&(data[i][1][0] === 2)) {
            priorities.push([4, "move", from, data[i][1][1]]);
        }
        if ((data[i][0] !== 0)&&((data[i][1] !== 0))&&(data[i][0][0] > data[i][1][0])) {
            priorities.push([5, "move", from, data[i][0][1]]);
        }
        if ((data[i][0] !== 0)&&((data[i][1] !== 0))&&(data[i][0][0] < data[i][1][0])) {
            priorities.push([6, "move", from, data[i][0][1]]);
        }
        if ((data[i][0] === 0)&&(data[i][1] !== 0)&&(!isBoss)) {
            priorities.push([7, "move", from, data[i][1][1]]);
        }
        if (((data[i][2] !== 0))&&((data[i][2][0] === 1)||(isBoss))) {
            priorities.push([8, "attack", from, data[i][2][1]]);
        }
        if ((data[i][0] !== 0)&&(data[i][1] === 0)) {
            priorities.push([9, "move", from, data[i][0][1]]);
        }
    }
    return priorities;
}

Qt.include("qrc:/js/timer.js")
Qt.include("qrc:/js/highlights.js")
Qt.include("qrc:/js/ai.js")

var who;
var tileItems = [];
var lastClick;
var controlUnit;
var gameType;
var wasted;
var teamEnergy = {"blue": 6, "green": 6};
var step = 0;

var unitComponent = Qt.createComponent("qrc:/qml/Unit.qml");
var dummyComponent = Qt.createComponent("qrc:/qml/Dummy.qml");
var wallComponent = Qt.createComponent("qrc:/qml/Wall.qml");
var bulletComponent = Qt.createComponent("qrc:/qml/Bullet.qml");
var giftComponent = Qt.createComponent("qrc:/qml/Gift.qml");
var healthComponent = Qt.createComponent("qrc:/qml/Health.qml");

function init(gt) {
    var type;
    var team;
    var isBoss;
    var countWalls = 0;

    gameType = gt;

    board.energyBlue = teamEnergy["blue"];
    board.energyGreen = teamEnergy["green"];

    for (var i=0; i<64; i++) {
        type = "dummy";
        team = "";
        isBoss = false;

        if ((i>=0)&&(i<8)) {
            type = "unit";
            team = "blue";
        } else if ((i>=56)&&(i<64)) {
            type = "unit";
            team = "green";
        } else {
            if ((i >= 16)&&(i < 48)&&(Math.random() > 0.6)&&(countWalls <= 24)) {
                type = "wall";
                countWalls += 1;
            }
        }

        if ((i === 0)||(i === 7)||(i === 56)||(i === 63))
        {
            isBoss = true;
        }

        tileItems[i] = createTileObject(i, type, team, isBoss);
    }
    who = "green";


    if (gameType === 0) {
        ai_vs_ai();
    }

    highlightTeam();
    greenInfoBar.state = "turn";
    greenScoreBar.state = "info";
}

function ai_vs_ai() {
    delay(700, function() {

        // принудительная остановка, пользователь нажал "назад"
        if (gameType !== 0) {
            return;
        }

        ai_start(who);
        checkFinish();
        if (!wasted) {
            ai_vs_ai();
        }

    });
}

function stop() {
    gameType = null;
}

function createTileObject(ind, type, team, isBoss) {
    var tile;

    switch (type) {
    case "dummy":
        tile = dummyComponent.createObject(
                    viewd, {
                        "x": cells.itemAt(ind).x,
                        "y": cells.itemAt(ind).y,
                        "color": (team) ? team : "#fff"
                    }
                    );
        break;

    case "wall":
        tile = wallComponent.createObject(
                    viewd, {
                        "x": cells.itemAt(ind).x,
                        "y": cells.itemAt(ind).y
                    }
                    );
        break;

    case "unit":
        tile = unitComponent.createObject(
                    viewd, {
                        "x": cells.itemAt(ind).x,
                        "y": cells.itemAt(ind).y,
                        "xp": (isBoss) ? 4 : 9,
                        "team": team,
                        "isBoss": isBoss
                    }
                    );
        break;

    case "gift":
        tile = giftComponent.createObject(
                    viewd, {
                        "x": cells.itemAt(ind).x,
                        "y": cells.itemAt(ind).y,
                        "variant": (team) ? team : "#fff"
                    }
                    );
        break;

    }

    if (tile === null) {
        console.log("Error creating a new tile");
    }

    return tile;
}


function setControl(ind) {
    controlUnit = ind;
    highlightActions(ind);
}


function click(ind) {
    if (controlUnit !== undefined) {

        if ((tileItems[ind].type === "unit")||tileItems[ind].type === "wall"||tileItems[ind].type === "gift") {

            if ((tileItems[ind].type === "unit")&&(tileItems[ind].team === who)&&(tileItems[ind].xp > 0)) {
                setControl(ind);
                setInfo(ind);
            } else {
                console.debug('check and attack');
                if (checkAttackUnit(controlUnit, ind)) {
                    attack(controlUnit, ind);
                    if (teamEnergy[who] > 0) {
                        highlightActions(controlUnit);
                    } else {
                        changeTeamEnergy();
                        changeTeam();

                        if (gameType === 1) {
                            delay(300, function() {
                                ai_start(who);
                            });
                        }
                    }
                }
            }

        } else {
            console.debug('check and move');
            if (checkMoveUnit(controlUnit, ind)) {
                moveUnit(controlUnit, ind);
                if (teamEnergy[who] > 0) {
                    highlightActions(ind);
                } else {
                    changeTeamEnergy();
                    changeTeam();

                    if (gameType === 1) {
                        delay(300, function() {
                            ai_start(who);
                        });
                    }
                }
            }
        }

    } else {

        if ((tileItems[ind].team === who)&&(tileItems[ind].xp > 0)) {
            setControl(ind);
            setInfo(ind);
        }

    }

    checkFinish();

    lastClick=ind;
    return (tileItems[ind]);
}


// проверка возможности перемещения юнита в конечную точку
// если передан третий параметр как true, то проверяется возможность
// первого шага из точки А по пути в точку В
function checkMoveUnit(from, to, oneStep) {
    if (tileItems[from].xp <= 0) return false;

    var requestEnergy;

    if (tileItems[from].isBoss) {
        requestEnergy = 2;
    } else {
        requestEnergy = 1;
    }

    if (oneStep) {
        if (from < to) {
            if (Math.floor(from/8)===Math.floor(to/8)) {
                to = from+1;
            } else {
                to = from+8;
            }
        } else if (from > to) {
            if (Math.floor(from/8)===Math.floor(to/8)) {
                to = from-1;
            } else {
                to = from-8;
            }
        } else {
            return false;
        }
    }

    if ((requestEnergy <= teamEnergy[who])&&
            (tileItems[to].type === "dummy")&&(
                (tileItems[to].highlightType === "vectorMove")||
                (tileItems[to].highlightType === "vectorCombine"))) {
        return (oneStep) ? to : true;
    } else {
        return false;
    }
}


function moveUnit(from, to) {

    // замена координат юнита на новые
    tileItems[from].x = cells.itemAt(to).x;
    tileItems[from].y = cells.itemAt(to).y;

    // запускается таймер для удаления пустой клетки
    tileItems[to].destroyContainer();
    // и пока ждём, переназначается объект с пустой клетки на бота
    tileItems[to] = tileItems[from];

    // в исходной точке же снова создаём пустую клетку
    tileItems[from] = createTileObject(from, "dummy", "#eceff1");

    controlUnit=to;

    for (var i=0; i<64; i++) {
        if (tileItems[i].type==="unit") {
            tileItems[i].highlightType="idle";
            tileItems[i].updateHighlight();
        }
    }
    tileItems[to].highlightType="control";
    tileItems[to].updateHighlight();

    var how = Math.abs(from%8 - to%8) + Math.floor(Math.abs(from/8 - to/8));
    console.debug("Ход на "+how+" клеток");

    var requestEnergy;
    if (tileItems[to].isBoss) {
        requestEnergy = 2;
    } else {
        requestEnergy = 1;
    }
    changeTeamEnergy(requestEnergy*how);
}


function checkAttackUnit(from, to) {
    if (tileItems[from].xp <= 0) return false;

    var requestEnergy;

    if (tileItems[from].isBoss) {
        requestEnergy = 4;
    } else {
        requestEnergy = 2;
    }

    if (    (requestEnergy <= teamEnergy[who])&&
            (
                (tileItems[to].type === "unit")&&(tileItems[to].team !== who)||
                (tileItems[to].type === "wall")||
                (tileItems[to].type === "gift")
            )&&
            (tileItems[to].highlightType === "underAttack")) {
        return true;
    } else {
        return false;
    }
}


function attack(from, to, gift_boom) {

    var minus;
    var variant;
    if (gift_boom === 1) {
        minus = 3;
    } else {
        minus = (tileItems[from].isBoss) ? 3 : 1;
    }

    for (var i=0; i<minus; i++) {
        if (tileItems[to].xp > 0) {

            tileItems[to].xp = tileItems[to].xp-1;
            if (tileItems[to].type === "unit") {
                tileItems[to].delEnergyBundle(getRandomRound(0, (tileItems[to].isBoss) ? 3 : 8));
            }
        }
        if ((tileItems[to].type === "unit")&&(tileItems[to].xp <= 0)) {
            tileItems[to].type = "wall";
            tileItems[to].xp = 4;
            tileItems[to].team = "";
        }
    }

    if ((tileItems[to].type === "wall")&&(tileItems[to].xp <= 0)) {
        tileItems[to] = createTileObject(to, "dummy");
    }

    if ((tileItems[to].type === "gift")&&(tileItems[to].xp <= 0)) {
        variant = tileItems[to].variant;
        tileItems[to] = createTileObject(to, "dummy");
        console.debug("BOOM", variant);
        if (variant === "dynamite") {
            gift_dynamite(to);
        } else {
            gift_health(to);
        }
    }

    var offset = cells.itemAt(from).width/2-u.dp(16);
    var bullet = bulletComponent.createObject(
                viewd, {
                    "x": cells.itemAt(from).x+offset,
                    "y": cells.itemAt(from).y+offset
                }
                );
    bullet.x = cells.itemAt(to).x+offset;
    bullet.y = cells.itemAt(to).y+offset;

    if (gift_boom === 1) return;

    var requestEnergy;
    if (tileItems[from].isBoss) {
        requestEnergy = 4;
    } else {
        requestEnergy = 2;
    }
    changeTeamEnergy(requestEnergy);
}


function changeTeam() {

    setInfo();

    controlUnit = undefined;

    if (who === "blue") {
        who = "green";
    } else {
        who = "blue";
    }
    step++

    if (step%9 === 0) {
        sendGift();
    }

    highlightTeam(true);
}


function checkFinish() {
    wasted = true;
    for (var i=0; i<64; i++) {
        if ((tileItems[i].type === "unit")&&(tileItems[i].team !== who)&&(tileItems[i].xp > 0)) {
            wasted = false;
            break;
        }
    }
    if (wasted) {
        board.isFinish = true;

        if (who === "blue") {
            board.winner = "синие";
        } else {
            board.winner = "зелёные";
        }
    }
}


// Функция получения случайного целого числа
// в диапазоне чисел включительно
function getRandomRound(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}


function changeTeamEnergy(how) {
    if (how !== undefined) {
        teamEnergy[who] -= how;

        if (who === "blue") {
            blueScoreBar.addScoreText = "-"+how.toString();
            blueScoreBar.addScoreAnimIsRunning = true;
        } else {
            greenScoreBar.addScoreText = "-"+how.toString();
            greenScoreBar.addScoreAnimIsRunning = true;
        }
    } else {
        teamEnergy[who] = 6;
    }

    board.energyBlue = teamEnergy["blue"];
    board.energyGreen = teamEnergy["green"];
}

function setInfo(ind) {
    if (ind === undefined) {

        if (who === "blue") {
            blueInfoBar.state = "gray";
            greenInfoBar.state = "turn";

            blueScoreBar.state = "gray";
            greenScoreBar.state = "info";
        } else {
            blueInfoBar.state = "turn";
            greenInfoBar.state = "gray";

            blueScoreBar.state = "info";
            greenScoreBar.state = "gray";
        }

    } else {

        var isBoss = tileItems[ind].isBoss;
        var xp = tileItems[ind].xp;
        var move;
        var attack;
        var damage;
        if (isBoss) {
            xp += "/4";
            move = "2";
            attack = "4";
            damage = "3";
        } else {
            xp += "/9";
            move = "1";
            attack = "2";
            damage = "1";
        }

        if (who === "blue") {
            blueInfoBar.setInfoBotIsBoss = isBoss;
            blueInfoBar.setInfoBotXP = xp;
            blueInfoBar.setInfoBotMove = move;
            blueInfoBar.setInfoBotAttack = attack;
            blueInfoBar.setInfoBotDamage = damage;

            blueInfoBar.state = "info";

            blueScoreBar.state = "info";
            greenScoreBar.state = "gray";
        } else {
            greenInfoBar.setInfoBotIsBoss = isBoss;
            greenInfoBar.setInfoBotXP = xp;
            greenInfoBar.setInfoBotMove = move;
            greenInfoBar.setInfoBotAttack = attack;
            greenInfoBar.setInfoBotDamage = damage;

            greenInfoBar.state = "info";

            blueScoreBar.state = "gray";
            greenScoreBar.state = "info";
        }
    }
}

function sendGift() {
    var i;
    var free_cells = 0;
    for (i=0; i<64; i++) {
        if (tileItems[i].type === "dummy") {
            free_cells++;
        }
    }
    if (free_cells < 4) return;

    var gift_pos = getRandomRound(0, free_cells);

    for (i=0; i<64; i++) {
        if (gift_pos > 0) {
            if (tileItems[i].type === "dummy") {
                gift_pos--;
            }
        }
        if (gift_pos === 0) {
            gift_pos = i;
            break;
        }
    }

    var rnd = getRandomRound(0, 1);
    var gift_variant = ((rnd === 1) ? "dynamite" : "health");

    if (tileItems[gift_pos].type !== "dummy") {
        console.debug("TODO: попытка отправки подарка в занятую клетку");
        sendGift();
        return;
    }
    console.debug("Вас ждёт подарок в клетке", gift_pos, tileItems[gift_pos].type);
    tileItems[gift_pos].destroyContainer();
    tileItems[gift_pos] = createTileObject(gift_pos, "gift", gift_variant);
}

function gift_dynamite(from) {
    var i;
    for (i=0; i<64; i++) {
        tileItems[i].highlightType="idle";
    }

    highlightOneAround(from, 1);
    for (i=0; i<64; i++) {
        if ((tileItems[i].highlightType === "vectorAttack")||
            (tileItems[i].highlightType === "underAttack")) {

            attack(from, i, 1);
        }
    }
}

function gift_health(from) {
    var i;
    var add_energy;
    var how;
    for (i=0; i<64; i++) {
        tileItems[i].highlightType="idle";
    }
    highlightOneAround(from, 1);
    for (i=0; i<64; i++) {
        if ((tileItems[i].highlightType === "underAttack")&&(tileItems[i].type !== "gift")) {
            how = tileItems[i].xp;
            tileItems[i].xp += 3;

            switch (tileItems[i].type) {
            case "unit":
                if (tileItems[i].isBoss) {
                    tileItems[i].xp = ((tileItems[i].xp > 4) ? 4 : tileItems[i].xp);
                } else {
                    tileItems[i].xp = ((tileItems[i].xp > 9) ? 9 : tileItems[i].xp);
                }
                how = tileItems[i].xp-how;
                for (add_energy=0; add_energy<how; add_energy++) {
                    console.debug("+", add_energy, how);
                    tileItems[i].addEnergyBundle(getRandomRound(0, (tileItems[i].isBoss) ? 3 : 8));
                }

                break;

            case "wall":
                tileItems[i].xp = ((tileItems[i].xp > 4) ? 4 : tileItems[i].xp);
                break;
            }

            var offset = cells.itemAt(from).width/2-u.dp(16);
            var bullet = healthComponent.createObject(
                        viewd, {
                            "x": cells.itemAt(from).x+offset,
                            "y": cells.itemAt(from).y+offset
                        }
                        );
            bullet.x = cells.itemAt(i).x+offset;
            bullet.y = cells.itemAt(i).y+offset;

        }
    }
}

function Timer() {
    return Qt.createQmlObject("import QtQuick 2.0; Timer {}", board);
}

function delay(delayTime, cb) {
    var timer = new Timer();
    timer.interval = delayTime;
    timer.repeat = false;
    timer.triggered.connect(cb);
    timer.start();
}

function side(v1, v2, coal) {
    return (Math.abs(v1-v2)<coal);
}


// передача управления подсвечиваемым действия функциям
function highlightActions(ind) {
    var i, energy;

    for (i=0; i<64; i++) {
        tileItems[i].highlightType="idle";
        if ((tileItems[i].type === "unit")&&(tileItems[i].team === who)) {
            tileItems[i].highlightType="team";
        }
    }

    if (ind === undefined) {
        return;
    }

    energy = teamEnergy[who];

    var isBoss = tileItems[ind].isBoss;
    if (isBoss === true) {
        if (energy >= 4) {
            highlightRadius(ind, 4);
        }
    } else {
        if (energy >= 2) {
            highlightOneAround(ind);
        }
    }

    // подсветка, куда можно ходить
    var minus = (isBoss === true) ? 2 : 1;
    for (i=0; i<4; i++) {
        highlightVector(ind, i, energy, minus);
    }

    tileItems[ind].highlightType = "controlMove";

    for (i=0; i<64; i++) {
        tileItems[i].updateHighlight();
    }
}


// рекурсивный алгоритм для подсветки путей отхода
function highlightVector(old_pos, vector, energy, minus) {
    if (energy<minus) return;
    var new_pos;

    // север
    if (vector===0) {
        new_pos=old_pos-8;
    }
    // восток
    if (vector===1) {
        new_pos=old_pos+1;
        if ( Math.floor((new_pos)/8)!=Math.floor((old_pos)/8) ) {
            return;
        }
    }
    // юг
    if (vector===2) {
        new_pos=old_pos+8;
    }
    // запад
    if (vector===3) {
        new_pos=old_pos-1;
        if ( Math.floor((new_pos)/8)!=Math.floor((old_pos)/8) ) {
            return;
        }
    }

    if ((new_pos>=0) && (new_pos<64))
    {
        if (tileItems[new_pos].type==="dummy")
        {
            // можно и ходить, и стрелять в ту сторону
            if (tileItems[new_pos].highlightType === "vectorAttack") {
                tileItems[new_pos].highlightType = "vectorCombine";
            } else {
                tileItems[new_pos].highlightType = "vectorMove";
            }
        } else {
            return;
        }

        highlightVector(new_pos, vector, energy-minus, minus);
    }
}


// подсветка области атаки пешек, на одну клетку во все стороны
function highlightOneAround(ind, all_teams) {
    var arr = [ind-9, ind-8, ind-7, ind-1, ind+1, ind+7, ind+8, ind+9];
    for (var i in arr) {

        // если больше размера доски или выходит за боковые границы
        if ( (arr[i] < 0)||(arr[i] >= 64)||Math.abs(ind%8-arr[i]%8)>1 ) {
            continue;
        }

        if (tileItems[arr[i]].type === "dummy") {
            tileItems[arr[i]].highlightType = "vectorAttack";
        } else if (
                       (tileItems[arr[i]].type === "wall") ||
                       (tileItems[arr[i]].type === "gift") ||
                       (
                           (tileItems[arr[i]].type === "unit")&&
                           ((tileItems[arr[i]].team !== who)||(all_teams === 1))&&
                           (tileItems[arr[i]].xp > 0)
                       )
                   ) {
            tileItems[arr[i]].highlightType = "underAttack";
        }
    }
}


// алгоритм для подсветки атаки по радиусу
function highlightRadius(old_pos, energy) {
    if (energy<1)
        return;

    var i, cos, sin, len;
    var barrier = {};
    var magicNum = 0.15;

    var pos_x = Math.floor(old_pos/8);
    var pos_y = old_pos%8;

    for (i=0; i<64; i++) {

        if (i===old_pos) {
            continue;
        }

        len = Math.sqrt( Math.pow(pos_x-Math.floor(i/8), 2) + Math.pow(pos_y-i%8, 2) );
        if ( len<=energy ) {
            if (tileItems[i].type!=="dummy") {
                cos = (pos_y-i%8) / len;
                sin = Math.sqrt( 1 - cos*cos );
                if (old_pos<i) {
                    sin = -sin;
                }
                barrier[i]=[cos, sin, len];
            } else {
                tileItems[i].highlightType = "vectorAttack";
                tileItems[i].updateHighlight();
            }
        }
    }
    for (i=0; i<64; i++) {
        if (tileItems[i].highlightType === "shadow")
            continue;

        if ((tileItems[i].type === "unit")&&(tileItems[i].team === who)) {
            tileItems[i].highlightType = "team";
            tileItems[i].updateHighlight();
            continue;
        }

        len = Math.sqrt( Math.pow(pos_x-Math.floor(i/8), 2) + Math.pow(pos_y-i%8, 2) );
        if ( len <= energy ) {
            cos = (pos_y-i%8) / len;
            sin = Math.sqrt( 1 - cos*cos );

            if (old_pos < i) {
                sin = -sin;
            }

            for (var el in barrier) {

                if ( side(cos,barrier[el][0],magicNum) && side(sin,barrier[el][1],magicNum) && (len>barrier[el][2]) ) {
                    tileItems[i].highlightType = "shadow";
                } else if ( tileItems[i].highlightType !== "shadow" && side(cos,barrier[el][0],magicNum) && side(sin,barrier[el][1],magicNum) && (len===barrier[el][2]) && (tileItems[i].xp > 0) ) {
                    tileItems[i].highlightType = "underAttack";
                }

            }
        }
        tileItems[i].updateHighlight();
    }
}


// подсветка текущей команды
// changed: реинициализация после смены команды
function highlightTeam(changed) {

    for (var i=0; i<64; i++) {
        if ((!changed)&&(controlUnit === i)) {
            continue;
        }

        if (tileItems[i].type === "unit") {
            /*
            if (tileItems[i].highlightType==="control")
                continue;*/
            if ((tileItems[i].team === who)&&(tileItems[i].xp > 0)) {
                tileItems[i].highlightType = "team";
            } else {
                tileItems[i].highlightType = "idle";
            }
        } else if ((
                       (tileItems[i].type === "dummy")||
                       (tileItems[i].type === "wall")||
                       (tileItems[i].type === "gift")
                   )&&(changed)) {
            tileItems[i].highlightType = "idle";
        }
        tileItems[i].updateHighlight();
    }
}

TEMPLATE = app

QT += qml quick

CONFIG += c++11

# Default rules for deployment.
include(deployment.pri)

android {
    QT += androidextras xml svg

    DISTFILES += \
        android/AndroidManifest.xml \
        android/gradle/wrapper/gradle-wrapper.jar \
        android/gradlew \
        android/res/values/libs.xml \
        android/build.gradle \
        android/gradle/wrapper/gradle-wrapper.properties \
        android/gradlew.bat \
        android/src/org/exarhteam/badsquare/StatusBarActivity.java
}

CONFIG += c++11

SOURCES += main.cpp \
    DeviceInfo.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    DeviceInfo.h

DISTFILES +=

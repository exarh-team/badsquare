import QtQuick 2.5
import QtQuick.Layouts 1.3

Item {
    id: cont
    property alias mouseArea: mouseArea
    property alias energy: energy.text
    property bool isRotated: false

    property alias addScoreText: addScoreText.text
    property alias addScoreAnimIsRunning: addScoreAnim.running

    property int animationTime: 200
    state: "gray"

    Item {
        x: u.dp(11)
        y: x
        width: parent.width-u.dp(22)
        height: parent.height-u.dp(22)

        Rectangle {
            x: (isRotated) ? u.dp(-6) : u.dp(6)
            y: x
            width: parent.width
            height: parent.height
            color: "#09514a"
            radius: u.dp(2)
        }

        Rectangle {
            width: parent.width
            height: parent.height
            radius: u.dp(2)

            color: "#fff"

            Rectangle {
                id: scoreInfo
                anchors.fill: parent
                radius: u.dp(2)
                color: "#fff"

                ColumnLayout {
                    anchors.fill: parent

                    spacing: 0

                    Rectangle {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        color: "#eee"
                        radius: u.dp(2)

                        Item {
                            x: u.dp(7)
                            y: u.dp(7)

                            Text {
                                color: "#111"
                                font.family: localFont.name
                                font.pixelSize: u.dp(14)
                                text: "Очков действия:"
                            }
                            Text {
                                id: energy
                                color: "#111"
                                x: u.dp(114)
                                font.family: localFont.name
                                font.pixelSize: u.dp(14)


                                Text {
                                    id: addScoreText
                                    font.family: localFont.name
                                    font.pixelSize: u.dp(20)
                                    font.bold: true
                                    color: Qt.rgba(119/255, 110/255, 101/255, 0.9);
                                    anchors.horizontalCenter: parent.horizontalCenter

                                    property real yfrom: -4
                                    property real yto: -(parent.y + parent.height*2)
                                    property int addScoreAnimTime: 600

                                    ParallelAnimation {
                                        id: addScoreAnim
                                        running: false

                                        NumberAnimation {
                                            target: addScoreText
                                            property: "y"
                                            from: addScoreText.yfrom
                                            to: addScoreText.yto
                                            duration: addScoreText.addScoreAnimTime

                                        }
                                        NumberAnimation {
                                            target: addScoreText
                                            property: "opacity"
                                            from: 1
                                            to: 0
                                            duration: addScoreText.addScoreAnimTime
                                        }
                                    }
                                }


                            }
                        }


                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        Text {
                            color: "#111"
                            anchors.centerIn: parent
                            font.pixelSize: u.dp(14)
                            font.family: localFont.name
                            text: "Пропустить ход"
                        }
                    }
                }

                Behavior on opacity {
                    NumberAnimation {
                        duration: animationTime
                    }
                }
            }

            Rectangle {
                id: scoreGray
                anchors.fill: parent
                radius: u.dp(2)
                color: "#eee"

                Behavior on opacity {
                    NumberAnimation {
                        duration: animationTime
                    }
                }
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
            }

        }
    }

    states: [
        State {
            name: "info"
            PropertyChanges { target: scoreInfo; opacity: 1 }
            PropertyChanges { target: scoreGray; opacity: 0 }
            PropertyChanges { target: mouseArea; enabled: true }
        },
        State {
            name: "gray"
            PropertyChanges { target: scoreInfo; opacity: 0 }
            PropertyChanges { target: scoreGray; opacity: 1 }
            PropertyChanges { target: mouseArea; enabled: false }
        }
    ]

}


import QtQuick 2.5
import QtQuick.Layouts 1.3

Item {
    anchors.margins: u.dp(11)

    RowLayout {
        spacing: u.dp(11)
        anchors.fill: parent

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            FlatButton {
                x: u.dp(11)
                width: u.dp(163)
                height: u.dp(30)

                mText: "Назад"

                mouseArea.onClicked: {
                    board.stopGame();
                    var page = boardActivity.createObject();
                    pageView.pop(page);
                }
            }

        }


        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            FlatButton {
                width: u.dp(163)
                height: u.dp(30)

                mText: "Правила"

                mouseArea.onClicked: {
                    board.isRules = true;
                }
            }
        }
    }
}

import QtQuick 2.5
import QtQuick.Layouts 1.3

Item {
    property bool isRotated: false
    property string setInfoTeam: ""

    property bool setInfoBotIsBoss: false
    property alias setInfoBotXP: infoXP.text
    property alias setInfoBotMove: infoMove.text
    property alias setInfoBotAttack: infoAttack.text
    property alias setInfoBotDamage: infoDamage.text

    property int animationTime: 200
    //property string botImage: "qrc:/media/bot-" + setInfoTeam + ((setInfoBotIsBoss) ? "-boss" : "") + ".png"
    property string botImage: "qrc:/media/bots.svg"
    property int textSize: u.dp(13)

    state: "gray"

    Item {
        x: u.dp(11)
        y: x
        width: parent.width-u.dp(11)
        height: parent.height-u.dp(22)

        Rectangle {
            x: (isRotated) ? u.dp(-6) : u.dp(6)
            y: x
            width: parent.width
            height: parent.height
            color: "#09514a"
            radius: u.dp(2)
        }

        Rectangle {
            width: parent.width
            height: parent.height
            color: "#fff"
            radius: u.dp(2)

            Rectangle {
                id: stateInfo
                anchors.fill: parent
                radius: u.dp(2)
                color: "#fff"

                RowLayout {
                    anchors.fill: parent

                    Item {
                        Layout.preferredWidth: u.dp(80)
                        Layout.fillHeight: true

                        AnimatedSprite {
                            id: animatedSprite

                            anchors.centerIn: parent
                            width: parent.width-u.dp(20)
                            height: width

                            source: botImage
                            frameCount: 6
                            frameWidth: 100
                            frameHeight: 100
                            paused: true

                            currentFrame: (setInfoTeam === "green") ? ((setInfoBotIsBoss) ? 3 : 0) : ((setInfoBotIsBoss) ? 4 : 1)
                        }

                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.preferredHeight: parent.height-u.dp(10)

                        ColumnLayout {
                            anchors.fill: parent

                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                Text {
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.verticalCenterOffset: -1
                                    color: "#111"
                                    font.family: localFont.name
                                    font.pixelSize: textSize
                                    text: "XP:"

                                    Text {
                                        id: infoXP
                                        x: parent.width+u.dp(5)
                                        color: "#111"
                                        font.family: localFont.name
                                        font.pixelSize: textSize
                                    }
                                }
                            }

                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                Text {
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.verticalCenterOffset: -1
                                    color: "#111"
                                    font.family: localFont.name
                                    font.pixelSize: textSize
                                    text: "Ход:"

                                    Text {
                                        id: infoMove
                                        x: parent.width+u.dp(5)
                                        color: "#111"
                                        font.family: localFont.name
                                        font.pixelSize: textSize
                                    }
                                }
                            }

                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                Text {
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.verticalCenterOffset: -1
                                    color: "#111"
                                    font.family: localFont.name
                                    font.pixelSize: textSize
                                    text: "Выстрел:"

                                    Text {
                                        id: infoAttack
                                        x: parent.width+u.dp(5)
                                        color: "#111"
                                        font.family: localFont.name
                                        font.pixelSize: textSize
                                    }
                                }

                            }

                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true

                                Text {
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.verticalCenterOffset: -1
                                    color: "#111"
                                    font.family: localFont.name
                                    font.pixelSize: textSize
                                    text: "Урон:"

                                    Text {
                                        id: infoDamage
                                        x: parent.width+u.dp(5)
                                        color: "#111"
                                        font.family: localFont.name
                                        font.pixelSize: textSize
                                    }
                                }

                            }

                        }
                    }
                }

                Behavior on opacity {
                    NumberAnimation {
                        duration: animationTime
                    }
                }
            }

            Rectangle {
                id: stateGray
                anchors.fill: parent
                radius: u.dp(2)
                color: "#eee"

                Behavior on opacity {
                    NumberAnimation {
                        duration: animationTime
                    }
                }
            }

            Rectangle {
                id: stateTurn
                anchors.fill: parent
                radius: u.dp(2)
                color: "#fff"

                Text {
                    color: "#111"
                    font.family: localFont.name
                    font.pixelSize: u.dp(20)
                    text: "Ваш ход"
                    anchors.centerIn: parent
                }

                Behavior on opacity {
                    NumberAnimation {
                        duration: animationTime
                    }
                }
            }
        }
    }

    states: [
        State {
            name: "info"
            PropertyChanges { target: stateInfo; opacity: 1 }
            PropertyChanges { target: stateGray; opacity: 0 }
            PropertyChanges { target: stateTurn; opacity: 0 }
        },
        State {
            name: "gray"
            PropertyChanges { target: stateInfo; opacity: 0 }
            PropertyChanges { target: stateGray; opacity: 1 }
            PropertyChanges { target: stateTurn; opacity: 0 }
        },
        State {
            name: "turn"
            PropertyChanges { target: stateInfo; opacity: 0 }
            PropertyChanges { target: stateGray; opacity: 0 }
            PropertyChanges { target: stateTurn; opacity: 1 }
        }
    ]

}

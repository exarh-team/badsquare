import QtQuick 2.3

Rectangle {
    id: container
    width: viewd.width/8-u.dp(1)
    height: width
    z: 1
    color: "#ffffff"
    border.color: "#ffffff"

    property int xp: 9
    property string type: "unit"
    property string team: ""
    property bool isBoss: false
    property int animationTime: 200
    property string highlightType: "idle"
    property bool destroyFlag: false

    property string botImage: "qrc:/media/bots.svg"
    property string botEnergy: "qrc:/media/energy.svg"

    Item {
        id: helper
        focus: false
        property var highlightColors: {"idle": "#ffffff",
                                       "shadow": "#ffffff",
                                       "team": "#d1dce3",          // подсветка хода команды
                                       "controlMove": "#d1dce3",   // подсветка юнита при управлении им для ходьбы
                                       "underAttack": "#ffc8c8",   // подсветка тайла находящегося под угрозой атаки
                                       "underAttackBorder": "#fe5155"
        }
    }

    AnimatedSprite {
        id: animatedSprite

        width: parent.width
        height: parent.width

        source: botImage
        frameCount: 6
        frameWidth: 100
        frameHeight: 100
        paused: true

        opacity: 1
        currentFrame: (team === "green") ? ((isBoss) ? 3 : 0) : ((isBoss) ? 4 : 1)

        Behavior on opacity {
            NumberAnimation {
                duration: 100
            }
        }
    }

    AnimatedSprite {
        id: destroyed

        width: parent.width
        height: parent.width

        source: botImage
        frameCount: 6
        frameWidth: 100
        frameHeight: 100
        paused: true

        opacity: 0
        currentFrame: (isBoss) ? 5 : 2

        Behavior on opacity {
            NumberAnimation {
                duration: 100
            }
        }
    }

    // изображение разрушения блока
    Item {
        width: parent.width
        height: width

        Image {
            id: "destruction1"
            source: "qrc:/media/destruction-unit-1.png"
            width: parent.width
            height: width
            smooth: false
            opacity: 0

            Behavior on opacity {
                NumberAnimation {
                    duration: animationTime
                }
            }
        }

        Image {
            id: "destruction2"
            source: "qrc:/media/destruction-unit-2.png"
            width: parent.width
            height: width
            smooth: false
            opacity: 0

            Behavior on opacity {
                NumberAnimation {
                    duration: animationTime
                }
            }
        }

        Image {
            id: "destruction3"
            source: "qrc:/media/destruction-unit-3.png"
            width: parent.width
            height: width
            smooth: false
            opacity: 0

            Behavior on opacity {
                NumberAnimation {
                    duration: animationTime
                }
            }
        }

    }

    // модель с энергией бота
    Grid {
        id: energy
        anchors.centerIn: parent

        rows: ((isBoss) ? 2 : 3)
        columns: rows

        width: rows*u.dp(7)
        height: width

        clip: true

        Repeater {
            id: cells
            model: ((isBoss) ? 4 : 9)

            AnimatedSprite {
                width: u.dp(7)
                height: width

                source: botEnergy
                frameCount: 2
                frameWidth: 10
                frameHeight: 10
                paused: true

                currentFrame: (team === "green") ? 0 : 1
            }

        }
    }

    Behavior on color {
        ColorAnimation {
            duration: animationTime
        }
    }

    Behavior on x {
        NumberAnimation {
            duration: animationTime
            onRunningChanged: {
                if ((!running) && destroyFlag) {
                    container.destroy();
                }
            }
        }
    }

    Behavior on y {
        NumberAnimation {
            duration: animationTime
            onRunningChanged: {
                if ((!running) && destroyFlag) {
                    container.destroy();
                }
            }
        }
    }

    Behavior on border.color {
        ColorAnimation {
            duration: animationTime
        }
    }

    Behavior on border.width {
        NumberAnimation {
            duration: animationTime
        }
    }

    onXpChanged: {
        for (var i=0; i<=3; i++) {
            destruction1.opacity = 0;
            destruction2.opacity = 0;
            destruction3.opacity = 0;
        }

        if (type==="unit") {
            if (xp <= 0) {
                destroyed.opacity = 1
                animatedSprite.opacity = 0
            }
        } else {

            if (xp <= 0) {
                container.opacity = 0;
            } else {
                if (xp === 3) {
                    destruction3.opacity = 1;
                }
                if (xp === 2) {
                    destruction2.opacity = 1;
                }
                if (xp === 1) {
                    destruction1.opacity = 1;
                }
            }

        }
    }

    onOpacityChanged: {
        if ((opacity === 0)&&(type === "wall")){
            container.destroy();
        }
    }

    function updateHighlight() {
        if (highlightType in helper.highlightColors) {
            container.color=helper.highlightColors[highlightType];
        }
        if (highlightType === "underAttack") {
            container.border.color = helper.highlightColors["underAttackBorder"];
            container.border.width = u.dp(2);
        } else {
            container.border.color = helper.highlightColors["idle"];
            container.border.width = 0;
        }
    }

    function delEnergyBundle(ind, recurs) {
        // если клетка уже скрыта...
        if (cells.itemAt(ind).opacity === 0) {
            // ...и это первый вызов функции
            if (!recurs) {
                // начинаем с первой
                ind = 0;
            } else {
                ind += 1;
            }

            delEnergyBundle(ind, true);
        } else {
            cells.itemAt(ind).opacity = 0;
        }
    }


    function addEnergyBundle(ind, recurs) {
        // если клетка уже открыта...
        console.debug("add energy", ind);
        if (cells.itemAt(ind).opacity === 1) {
            // ...и это первый вызов функции
            if (!recurs) {
                // начинаем с первой
                ind = 0;
            } else {
                ind += 1;
            }

            addEnergyBundle(ind, true);
        } else {
            cells.itemAt(ind).opacity = 1;
        }
    }
}

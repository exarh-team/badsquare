import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import "qrc:/js/game.js" as MyScript

Item {
    id: board

    property alias energyBlue: blueScoreBar.energy
    property alias energyGreen: greenScoreBar.energy
    property bool isFinish: false
    property bool isRules: false
    property alias winner: finish.winner
    property int gameType: 1

    function stopGame() {
        MyScript.stop();
    }

    Rectangle {
        anchors.fill: parent

        Item {
            id: helper
            focus: false
            property var myColors: {"bglight": "#13a395",
                                    "bggray": "#333", //Qt.rgba(238/255, 228/255, 218/255, 0.35),
                                    "bgdark": "#BBADA0",
                                    "fglight": "#EEE4DA",
                                    "fgdark": "#776E62",
                                    "bgbutton": "#8F7A66", // Background color for the "New Game" button
                                    "fgbutton": "#F9F6F2" // Foreground color for the "New Game" button
            }
        }
        color: helper.myColors.bglight

        Rectangle {
            id: mainWindow
            width: u.dp(360)
            height: u.dp(640)
            anchors.centerIn: parent

            Rectangle {
                id: gameActivity
                width: u.dp(360)
                height: u.dp(640)
                color: helper.myColors.bglight

                ColumnLayout {
                    id: content
                    width: u.dp(360)
                    height: u.dp(536)
                    spacing: 0

                    anchors.centerIn: parent

                    RowLayout {
                        spacing: 0
                        Layout.fillWidth: true
                        Layout.preferredHeight: u.dp(75)
                        rotation: 180

                        InfoBar {
                            id: blueInfoBar
                            Layout.preferredWidth: u.dp(200)
                            Layout.fillHeight: true
                            isRotated: true
                            setInfoTeam: "blue"
                            visible: (gameType === 2) ? 1 : 0
                        }

                        ScoreBar {
                            id: blueScoreBar
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            isRotated: true
                            visible: (gameType === 2) ? 1 : 0

                            mouseArea.onClicked: {
                                MyScript.changeTeamEnergy();
                                MyScript.changeTeam();
                            }
                        }
                    }

                    Item {
                        Layout.fillWidth: true
                        Layout.preferredHeight: u.dp(336)

                        Item {
                            width: u.dp(336)
                            height: width
                            anchors.centerIn: parent

                            Rectangle {
                                x: u.dp(6)
                                y: x
                                width: parent.width
                                height: parent.width
                                color: "#09514a"
                                radius: u.dp(2)
                            }

                            Rectangle {
                                id: field

                                width: parent.width
                                height: parent.width
                                radius: u.dp(2)

                                Grid {
                                    id: viewd
                                    spacing: u.dp(1)
                                    anchors.fill: parent
                                    rows: 8
                                    columns: 8

                                    Repeater {
                                        id: cells
                                        model: 64

                                        Rectangle {
                                            color: "#666"
                                            width: viewd.width/8-u.dp(1)
                                            height: viewd.width/8-u.dp(1)

                                            MouseArea {
                                                id: mouseArea;
                                                anchors.fill: parent

                                                onClicked: {
                                                    MyScript.click(model.index);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    RowLayout {
                        spacing: 0
                        Layout.fillWidth: true
                        Layout.preferredHeight: u.dp(75)

                        InfoBar {
                            id: greenInfoBar
                            Layout.preferredWidth: u.dp(200)
                            Layout.fillHeight: true
                            setInfoTeam: "green"
                            visible: ((gameType === 1)||(gameType === 2)) ? 1 : 0
                        }

                        ScoreBar {
                            id: greenScoreBar
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            visible: ((gameType === 1)||(gameType === 2)) ? 1 : 0

                            mouseArea.onClicked: {
                                MyScript.changeTeamEnergy();
                                MyScript.changeTeam();

                                if (MyScript.gameType === 1) {
                                    MyScript.ai_start("blue");
                                }
                            }
                        }
                    }

                    ControlBar {
                        Layout.fillWidth: true
                        Layout.preferredHeight: u.dp(30)
                    }

                }
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#80000000"
        visible: isFinish || isRules
    }

    Finish {
        id: finish
        visible: isFinish
    }

    Rules {
        id: rules
        visible: isRules
    }

    Component.onCompleted: MyScript.init(gameType);

}

import QtQuick 2.5

Rectangle {
    id: container
    width: viewd.width/8-u.dp(1)
    height: width
    color: "#ffffff"

    property string type: "dummy"
    property int animationTime: 200
    property string highlightType: "idle"

    Item {
        id: helper
        focus: false
        property var highlightColors: {"idle": "#ffffff",
                                       "shadow": "#ffffff",
                                       "vectorMove": "#dfe4e8",          // разметка для хода
                                       "vectorAttack": "#ffc8c8",         // разметка для стрельбы
                                       "vectorCombine": "#f0dcdd"         // для стрельбы и хода
        }
    }

    Timer {
        id: destroyTimer
        interval: animationTime
        running: false
        onTriggered: container.destroy()
    }


    Behavior on color {
        ColorAnimation {
            duration: animationTime
        }
    }

    function updateHighlight() {
        if (highlightType in helper.highlightColors) {
            container.color=helper.highlightColors[highlightType];
        }
    }

    function destroyContainer() {
        destroyTimer.running = true
    }
}

import QtQuick 2.5

Item {
    anchors.fill: parent
    property string winner: ""

    MouseArea {
        anchors.fill: parent
    }

    Item {
        width: u.dp(300)
        height: u.dp(500)
        anchors.centerIn: parent

        Rectangle {
            x: u.dp(6)
            y: x
            width: parent.width
            height: u.dp(400)
            color: "#80000000"
            radius: u.dp(2)
        }

        Rectangle {
            width: parent.width
            height: u.dp(400)
            color: "#fff"
            radius: u.dp(2)

            Text {
                width: parent.width-u.dp(30)
                height: parent.height-u.dp(30)
                color: "#111"
                font.pixelSize: u.dp(12)
                font.family: localFont.name
                text: "<b>Правила игры:</b><br><br>
На игровом 8x8 клеток расположены по 2 босса и по 6 пешек у обоих команд.<br><br>
Цель игры - уничтожить всех ботов противника.<br><br>
Игроки ходят попеременно, каждому даётся 6 очков действия, которые можно потратить на действия над юнитами.<br><br>
Пешки в начале имеют 9 XP, для передвижения на 1 клетку затрачивают 1 единицу энергии и выстреливают с единичным уроном, затрачивая на это 2 единицы энергии.<br><br>
Боссы в начале имеют 4 XP, для передвижения на 1 клетку затрачивают 2 единицы энергии и выстреливают с тройным уроном, затрачивая на это 4 единицы энергии."
                anchors.centerIn: parent
                wrapMode: Text.Wrap
            }

        }

        FlatButton {
            y: u.dp(450)
            width: parent.width
            height: u.dp(50)
            anchors.horizontalCenter: parent.horizontalCenter
            shadowTransparency: true

            mText: "Закрыть"

            mouseArea.onClicked: {
                board.isRules = false;
            }
        }
    }
}

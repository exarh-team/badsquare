import QtQuick 2.5

Rectangle {
    id: container
    width: viewd.width/8-u.dp(1)
    height: width
    z: 1
    color: "#ffffff"

    property int xp: 1
    property string type: "gift"
    property string variant: "health"
    property int animationTime: 300
    property string highlightType: "idle"

    Item {
        id: helper
        focus: false
        property var highlightColors: {"idle_health": "#fff",
                                       "idle_dynamite": "#c26e35",
                                       "underAttack": "#fe5155"
        }
    }

    Image {
        source: "qrc:/media/gift_"+variant+".svg"
        width: parent.width
        height: width
        smooth: false

        Behavior on opacity {
            NumberAnimation {
                duration: animationTime
            }
        }
    }

    Behavior on opacity {
        NumberAnimation {
            duration: animationTime
        }
    }

    Behavior on border.color {
        ColorAnimation {
            duration: animationTime
        }
    }

    Behavior on border.width {
        NumberAnimation {
            duration: animationTime
        }
    }

    onXpChanged: {
        if (xp <= 0) {
            container.opacity = 0;
            destroyContainer();
        }
    }

    Timer {
        id: destroyTimer
        interval: animationTime
        running: false
        onTriggered: container.destroy()
    }

    function updateHighlight() {
        if (highlightType === "underAttack") {
            container.color = helper.highlightColors["underAttack"];
        } else {
            container.color = helper.highlightColors["idle_"+variant];
        }
    }

    function destroyContainer() {
        destroyTimer.running = true
    }

}

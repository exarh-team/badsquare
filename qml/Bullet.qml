import QtQuick 2.5

Item {
    id: container
    width: u.dp(32)
    height: u.dp(32)
    z: 2

    property int animationTime: 50

    Image {
        id: bullet
        source: "qrc:/media/bullet.png"
        width: parent.width
        height: parent.width
        smooth: false
        opacity: 1
    }

    AnimatedImage {
        id: boom
        source: "qrc:/media/boom.gif"
        width: parent.width
        height: parent.width
        opacity: 1
    }

    Behavior on x {
        NumberAnimation {
            duration: animationTime
        }
    }

    Behavior on y {
        NumberAnimation {
            duration: animationTime
        }
    }

    Behavior on opacity {
        NumberAnimation {
            duration: 300
        }
    }

    Timer {
        interval: animationTime
        running: true
        onTriggered: {
            container.opacity = 0;
        }
    }

    Timer {
        interval: 500
        running: true
        onTriggered: container.destroy()
    }
}

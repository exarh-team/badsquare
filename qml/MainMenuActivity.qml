import QtQuick 2.5
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3

Rectangle {
    id: windowmenu
    visible: true

    Item {
        id: helper
        focus: false
        property var myColors: {"bglight": "#13a395",
                                "bgbutton": "#8F7A66", // Background color for the "New Game" button
                                "fgbutton": "#F9F6F2" // Foreground color for the "New Game" button
        }
    }
    color: helper.myColors.bglight


    Item {
        y: u.dp(50)
        width: u.dp(300)
        height: u.dp(50)
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: gameName
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: u.dp(55)
            font.bold: true
            font.family: localFont.name
            text: "Bad Square"
            color: "#fff"
        }
    }

    ColumnLayout {
        y: u.dp(150)
        spacing: u.dp(50)
        anchors.horizontalCenter: parent.horizontalCenter



        FlatButton {
            width: u.dp(300)
            height: u.dp(50)
            anchors.horizontalCenter: parent.horizontalCenter

            mText: "Игрок vs Компьютер"

            mouseArea.onClicked: {

                var page = boardActivity.createObject(parent, {"gameType": 1});
                pageView.push(page)

            }
        }

        FlatButton {
            width: u.dp(300)
            height: u.dp(50)
            anchors.horizontalCenter: parent.horizontalCenter

            mText: "Игрок vs Игрок"

            mouseArea.onClicked: {

                var page = boardActivity.createObject(parent, {"gameType": 2});
                pageView.push(page)

            }
        }

        FlatButton {
            width: u.dp(300)
            height: u.dp(50)
            anchors.horizontalCenter: parent.horizontalCenter

            mText: (enableSound) ? "Выключить звук" : "Включить звук"

            mouseArea.onClicked: {
                enableSound = (enableSound) ? false : true;
            }
        }

        Item {
            width: u.dp(300)
            height: u.dp(50)
            anchors.horizontalCenter: parent.horizontalCenter

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var page = boardActivity.createObject(parent, {"gameType": 0});
                    pageView.push(page)
                }
            }
        }
    }
}

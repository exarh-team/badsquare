import QtQuick 2.5

Rectangle {
    id: container
    width: viewd.width/8-u.dp(1)
    height: width
    color: "#607d8b"
    z: 1
    border.color: "#ffffff"

    property int xp: 4
    property string type: "wall"
    property int animationTime: 300
    property string highlightType: "idle"

    Item {
        id: helper
        focus: false
        property var highlightColors: {"idle": "#607d8b",
                                       "shadow": "#607d8b",
                                       "underAttackBorder": "#fe5155"
        }
    }

    // изображение разрушения блока
    Item {
        width: parent.width
        height: width

        Image {
            id: "destruction1"
            source: "qrc:/media/destruction-1.png"
            width: parent.width
            height: width
            smooth: false
            opacity: 0

            Behavior on opacity {
                NumberAnimation {
                    duration: animationTime
                }
            }
        }

        Image {
            id: "destruction2"
            source: "qrc:/media/destruction-2.png"
            width: parent.width
            height: width
            smooth: false
            opacity: 0

            Behavior on opacity {
                NumberAnimation {
                    duration: animationTime
                }
            }
        }

        Image {
            id: "destruction3"
            source: "qrc:/media/destruction-3.png"
            width: parent.width
            height: width
            smooth: false
            opacity: 0

            Behavior on opacity {
                NumberAnimation {
                    duration: animationTime
                }
            }
        }

    }

    Behavior on opacity {
        NumberAnimation {
            duration: animationTime
        }
    }

    Behavior on border.color {
        ColorAnimation {
            duration: animationTime
        }
    }

    Behavior on border.width {
        NumberAnimation {
            duration: animationTime
        }
    }

    onXpChanged: {
        for (var i=0; i<=3; i++) {
            destruction1.opacity = 0;
            destruction2.opacity = 0;
            destruction3.opacity = 0;
        }

        if (xp <= 0) {
            container.opacity = 0;
        } else {

            if (xp === 3) {
                destruction3.opacity = 1;
            }
            if (xp === 2) {
                destruction2.opacity = 1;
            }
            if (xp === 1) {
                destruction1.opacity = 1;
            }

        }
    }

    onOpacityChanged: {
        if (opacity === 0) {
            container.destroy();
        }
    }


    Timer {
        id: destroyTimer
        interval: animationTime
        running: false
        onTriggered: container.destroy()
    }

    function updateHighlight() {
        if (highlightType in helper.highlightColors) {
            container.color=helper.highlightColors[highlightType];
        }
        if (highlightType === "underAttack") {
            container.border.color = helper.highlightColors["underAttackBorder"];
            container.border.width = u.dp(2);
        } else {
            container.border.color = helper.highlightColors["idle"];
            container.border.width = 0;
        }
    }

    function destroyContainer() {
        destroyTimer.running = true
    }

}

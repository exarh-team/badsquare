import QtQuick 2.5

Item {
    anchors.fill: parent
    property string winner: ""

    MouseArea {
        anchors.fill: parent
    }

    Item {
        width: u.dp(300)
        height: width
        anchors.centerIn: parent

        Rectangle {
            x: u.dp(6)
            y: x
            width: parent.width
            height: u.dp(200)
            color: "#80000000"
            radius: u.dp(2)
        }

        Rectangle {
            width: parent.width
            height: u.dp(200)
            color: "#fff"
            radius: u.dp(2)

            Text {
                y: u.dp(60)
                color: "#111"
                font.pixelSize: u.dp(30)
                font.family: localFont.name
                text: "Победили "+winner+"!"
                anchors.centerIn: parent
            }

        }

        FlatButton {
            y: u.dp(250)
            width: parent.width
            height: u.dp(50)
            anchors.horizontalCenter: parent.horizontalCenter
            shadowTransparency: true

            mText: "Заново?"

            mouseArea.onClicked: {
                var page = boardActivity.createObject(parent, {"gameType": board.gameType});
                pageView.push(page);
            }
        }
    }
}

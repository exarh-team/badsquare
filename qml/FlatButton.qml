import QtQuick 2.5

Item {
    width: u.dp(100)
    height: u.dp(20)

    property string mText: "button"
    property alias mouseArea: mouseArea
    property bool shadowTransparency: false

    Item {
        anchors.fill: parent

        Rectangle {
            x: u.dp(6)
            y: x
            width: parent.width
            height: parent.height
            color: (shadowTransparency) ? "#80000000" : "#09514a"
            radius: u.dp(2)
        }

        Rectangle {
            anchors.fill: parent
            color: "#fff"
            radius: u.dp(2)

            Text {
                color: "#111"
                anchors.centerIn: parent
                font.pixelSize: u.dp(15)
                font.family: localFont.name
                text: mText
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
            }

        }
    }
}

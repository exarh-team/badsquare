import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtMultimedia 5.6

Window {

    property bool enableSound: true

    Scalable {
        id: u
    }

    visible: true
    width: u.dp(360)
    height: u.dp(640)
    title: qsTr("Bad Square")

    FontLoader {
        id: localFont;
        source: "qrc:///fonts/DroidSansFallback.ttf"
    }

    Component {
        id: boardActivity
        Board {}
    }

    StackView {
        id: pageView
        anchors.fill: parent

        initialItem: Item {
            width: parent.width
            height: parent.height

            MainMenuActivity {
                anchors.fill: parent
            }
        }
    }

    Audio {
        id: playMusic
        source: "qrc:///media/comix-zone.ogg"
        loops: Audio.Infinite
        volume: 0.5
        muted: !enableSound
    }

    Component.onCompleted: {
        playMusic.play()
    }

    Connections {
        target: Qt.application
        onStateChanged: {
            if (Qt.application.state === 0) {
                playMusic.pause()
            } else {
                playMusic.play()
            }
        }
    }

}
